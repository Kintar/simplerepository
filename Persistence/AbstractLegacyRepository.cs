﻿using System;
using System.Collections.Generic;
using Persistence.Legacy;

namespace Persistence
{
    public abstract class AbstractLegacyRepository<T, TKey> : ILegacyRepository<T, TKey> where T : class
    {
        protected IConnectionProvider ConnectionProvider;

        public abstract void Save(T entity);
        public abstract T Get(TKey id);
        public abstract IEnumerable<T> Get(params TKey[] ids);

        protected AbstractLegacyRepository(IConnectionProvider connectionProvider)
        {
            this.ConnectionProvider = connectionProvider;
        }

        public IEnumerable<TKey> QueryIDs(ILegacyQuery<T> query)
        {
            var self = (dynamic) this;
            return self.RunQuery((dynamic) query);
        }

        public IEnumerable<T> Query(ILegacyQuery<T> query)
        {
            var self = (dynamic)this;
            return Get(self.RunQuery((dynamic) query).ToArray());
        }

        protected IEnumerable<object> RunQuery(ILegacyQuery<T> queryParameters)
        {
            throw new UnimplementedQueryException(queryParameters);
        }

        public virtual void Delete(TKey key)
        {
            throw new NotImplementedException();
        }
    }
}