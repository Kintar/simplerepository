﻿
using System;
using System.Runtime.Remoting;

namespace Persistence
{
    public abstract class Entity : IEntity
    {
        private Guid id;

        public Guid Id {
            get { return id; }
            set {
                if (id != default(Guid))
                {
                    throw new InvalidIdAlterationException(this, value);
                }

                id = value;
                SetId();
            }
        }

        /// <summary>
        /// If your entity contains child objects, implement this method to update
        /// their parent ID reference when the ID on this entity is set
        /// </summary>
        protected virtual void SetId()
        {
            // Default implementation does nothing
        }

        public long Version { get; set; }
        
        public override bool Equals(object obj)
        {
            var other = obj as Entity;
            return other != null && other.id == id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}