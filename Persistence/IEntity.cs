﻿using System;

namespace Persistence
{
    public interface IEntity
    {
        Guid Id { get; }
        long Version { get; }
    }
}