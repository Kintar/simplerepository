﻿namespace Persistence
{
    public interface IQueryParameters<out T> where T : class, IEntity
    {
        // Marker interface for type safety
    }
}