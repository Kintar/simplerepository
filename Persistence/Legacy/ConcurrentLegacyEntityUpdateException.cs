﻿using System;

namespace Persistence.Legacy
{
    public class ConcurrentLegacyEntityUpdateException : Exception
    {
        public object Entity { get; protected set; }


        public ConcurrentLegacyEntityUpdateException(object e)
            : base(string.Format("Legacy {0} entity failed version check.  Cannot update.", e.GetType().Name))
        {
            Entity = e;
        }
    }
}