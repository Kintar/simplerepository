﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Persistence.Legacy
{
    public interface ILegacyRepository<TEntity, TKey> 
    {
        TEntity Get(TKey id);
        IEnumerable<TEntity> Get(params TKey[] ids);
        IEnumerable<TEntity> Query(ILegacyQuery<TEntity> query);
        IEnumerable<TKey> QueryIDs(ILegacyQuery<TEntity> query);
        void Save(TEntity entity);
        void Delete(TKey key);
    }
}
