﻿namespace Persistence
{
    public class NonDeletableEntityException : EntityException
    {
        public NonDeletableEntityException(IEntity e)
            : base(string.Format("Cannot delete entities of type '{0}'", e.GetType().Name), e.GetType(), e.Id)
        {
        }
    }
}