﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Persistence.Legacy;

namespace Persistence
{
    public class UnimplementedQueryException : Exception
    {
        public UnimplementedQueryException(IQueryParameters<IEntity> query) 
            : base(string.Format("No implementation found for query backed by parameters of type {0}", query.GetType()))
        {
        }

        public UnimplementedQueryException(ILegacyQuery<object> query)
            : base(string.Format("No implementation found for query backed by parameters of type {0}", query.GetType()))
        {
        }
    }
}
