# README #

### Overview ###

This is a simple repository pattern helper for .Net.  I'll put in more details as I get time.

### How do I get set up? ###

For GUID-identified entities:

* Entity classes must be subclasses of Entity, which provides Id and Version properties
* Repositories should subclass AbstractRepository<Entity>
* Implement Save, Get(Guid), Get(Guid[]), and Delete
* For queries
  * Implement IQueryParameter<Entity> as a DTO for query parameters
  * For each concrete IQueryParameter<Entity> class, your repository should implement a public
    IEnumerable<Guid> RunQuery(IQueryParameter<Entity>) method which returns the set of entity IDs
	matching the query.
  * Run your query by calling Query and passing in the parameters for the query you want to run.
    The abstract repository will handle passing the IDs into the Get(Guid[]) method

For entities with other types of identifiers:

* Entity classes can be anything
* Implement AbstractLegacyRepository<TEntity, TKey>
* Query parameters should implement ILegacyQuery<TEntity>
* All the rest is the same as for Guid-based entities
	
### Contribution guidelines ###

Coming Soon...
